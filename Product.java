package store;
import java.awt.Font;
import java.sql.SQLException;

import com.sun.javafx.geom.Rectangle;

import javafx.application.*;
import javafx.fxml.FXML;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.text.FontPosture;
import javafx.scene.text.Text;
public class Product extends Application {
	//private Image image=new Image("ch.jpg");
	private String name="Chi Toz Salty Chips";
	protected int price=1000;
	private String producer="che toz";
	private int rate=5;
	private int calories=1000;
	private Date date_of_manufacture=new Date(5,5,1396);
    private  Date expiration=new Date(5,5,1402);
    private Date date_of_adding;
    private String description="nothing";
    private Seller seller;
    private Comment[] comments;
    
    public Product(String name,int price,String producer,int rate,int calories,Date date_of_manufacture, Date dateadd,Date exp,String desp){
    	
    	this.setCalories(calories);
    	this.setName(name);
    	this.setProducer(producer);;
    	this.price=price;
    	this.setDate_of_adding(dateadd);
    	this.setDate_of_manufacture(date_of_manufacture);
    	this.setExpiration(exp);
    	this.description=desp;
    	this.setRate(rate);
    }
   // comments[0]=new Comment("It was awful",new Date(28,3,1402),new Perchaser("Ahmad") );
    private int numberofcomments=0;
    private Cellar cellar;
	private Stage arg0;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getProducer() {
		return producer;
	}
	public void setProducer(String producer) {
		this.producer = producer;
	}
	public int getCalories() {
		return calories;
	}
	public void setCalories(int calories) {
		this.calories = calories;
	}
	public Date getDate_of_manufacture() {
		return date_of_manufacture;
	}
	public void setDate_of_manufacture(Date date_of_manufacture) {
		this.date_of_manufacture = date_of_manufacture;
	}
	public Date getExpiration() {
		return expiration;
	}
	public void setExpiration(Date expiration) {
		this.expiration = expiration;
	}
	public Date getDate_of_adding() {
		return date_of_adding;
	}
	public void setDate_of_adding(Date date_of_adding) {
		this.date_of_adding = date_of_adding;
	}
	public int getRate() {
		return rate;
	}
	public void setRate(int rate) {
		this.rate = rate;
	}
	public static void main(String[] args){
		launch(args);
		
		
	}
	@Override
	public void start(Stage primaryStage) throws Exception {
		
		// TODO Auto-generated method stub
	
	int i;
	Button b= new Button("Add to cart");
	Button b1=new Button("Home");
	Button b2=new Button("Card");
	Button b3=new Button("Exit");
	b1.setLayoutX(90);
	b2.setLayoutX(460);
	b3.setLayoutX(860);
	b1.setLayoutY(50);
	b2.setLayoutY(50);
	b3.setLayoutY(50);
	Group root = new Group();
	
	Label l= new Label();
	l.setText(this.name);
	l.setLayoutX(100);
	l.setLayoutY(100);
	l.setTextFill(Color.WHITE);
	Label labels[]= new Label[7];
	for(i=0;i<7;i++){labels[i]= new Label();}
	
	labels[0].setText("price: "+String.valueOf(this.price));
	labels[1].setText("producer: "+this.producer);
	labels[2].setText("rate: "+String.valueOf(this.rate));
	labels[3].setText("calories: "+String.valueOf(this.calories));
	labels[4].setText("date of manufacture: "+String.valueOf(this.date_of_manufacture.getYear())+"/"+ String.valueOf(this.date_of_manufacture.getMonth())+"/"+String.valueOf(this.date_of_manufacture.getDay()));
	labels[5].setText("expiration: "+String.valueOf(this.expiration.getYear())+"/"+ String.valueOf(this.expiration.getMonth())+"/"+String.valueOf(this.expiration.getDay()));
	labels[6].setText("description: "+this.description);
	for(i=0;i<7;i++){
		labels[i].setTextFill(Color.WHITE);
		labels[i].setLayoutX(400);
		labels[i].setLayoutY(160+i*40);
		root.getChildren().addAll(labels[i]);
		
	}
	Rectangle rectangle1=new Rectangle();
	rectangle1.setBounds(90, 130, 900, 360);
	
	
	b.setLayoutX(800);
	b.setLayoutY(400);
	Line line1 = new Line();
	line1.setStartX(90);
	line1.setEndX(900);
	line1.setEndY(130);
	line1.setStartY(130);
	line1.setStroke(Color.AQUA);
	line1.setOpacity(0.5);
	Line line2 = new Line();
	line2.setStartX(90);
	line2.setEndX(900);
	line2.setEndY(470);
	line2.setStartY(470);
	line2.setStroke(Color.AQUA);
	line2.setOpacity(0.5);
	Line line3 = new Line();
	line3.setStartX(90);
	line3.setEndX(90);
	line3.setEndY(130);
	line3.setStartY(470);
	line3.setStroke(Color.AQUA);
	line3.setOpacity(0.5);
	Line line4 = new Line();
	line4.setStartX(900);
	line4.setEndX(900);
	line4.setEndY(130);
	line4.setStartY(470);
	line4.setStroke(Color.AQUA);
	line4.setOpacity(0.5);
	Line line5 = new Line();
	line5.setStartX(90);
	line5.setEndX(900);
	line5.setEndY(90);
	line5.setStartY(90);
	line5.setStroke(Color.AQUA);
	line5.setOpacity(0.5);
	Line line6 = new Line();
	line6.setStartX(900);
	line6.setEndX(900);
	line6.setEndY(90);
	line6.setStartY(130);
	line6.setStroke(Color.AQUA);
	line6.setOpacity(0.5);
	Line line7 = new Line();
	line7.setStartX(90);
	line7.setEndX(90);
	line7.setEndY(90);
	line7.setStartY(130);
	line7.setStroke(Color.AQUA);
	line7.setOpacity(0.5);
	
	Label c=new Label();
	c.setText("comments:");
	c.setLayoutY(480);
	c.setLayoutX(100);
	c.setTextFill(Color.BLUEVIOLET);
	Label com[] = new Label[this.numberofcomments];
	for(i=0;i<this.numberofcomments;i++){
		com[i].setText(String.valueOf(this.comments[i].getDate().getYear())+"/"+ String.valueOf(this.comments[i].getDate().getMonth())+"/"+String.valueOf(this.comments[i].getDate().getDay())+" "+this.comments[i].getPerchaser().getUsername()+":"+this.comments[i].getText());
		com[i].setLayoutX(100);
		com[i].setLayoutY(510+i*20);
		com[i].setTextFill(Color.WHITE);
		root.getChildren().addAll(com[i]);
		
	}
	
	//Pane pane= new HBox(15);
	//ImageView imm= new ImageView(this.image);
	//imm.setLayoutX(100);
	//imm.setLayoutY(140);
	//imm.setFitWidth(390);
	//imm.setFitHeight(450);
	//pane.getChildren().add(imm);
	
	
		
		
	     
		root.getChildren().addAll(b);
		root.getChildren().addAll(b1);
		root.getChildren().addAll(b2);
		root.getChildren().addAll(b3);

		root.getChildren().addAll(l);
		root.getChildren().addAll(c);
		
		root.getChildren().addAll(line1);
		root.getChildren().addAll(line2);
		root.getChildren().addAll(line3);
		root.getChildren().addAll(line4);
		root.getChildren().addAll(line5);
		root.getChildren().addAll(line6);
		root.getChildren().addAll(line7);
	    
		Scene scene= new Scene(root,1000,700,Color.BLACK);
		primaryStage.setScene(scene);
		primaryStage.show();
		
		System.out.println("vhbjn");
		
		
	}
    
	
}
