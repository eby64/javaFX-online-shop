[![Available_in](https://img.shields.io/badge/-Available%20in-555)]()
[![Windows](https://img.shields.io/badge/-WINDOWS-blue)](https://www.microsoft.com/en-us/windows)
[![Linux](https://img.shields.io/badge/-LINUX-blue)]()



[![Requirements](https://img.shields.io/badge/Requirements-gcc%20%2F%20git-blue)]()



# online-shop
simple online shop with javafx

# Requirements
* java20
* git

# installation
first, clone the repository

    git clone https://github.com/khodekhadem/javaFX-online-shop

then, move to the directory

    cd javaFX-online-shop

compile the sources with your ide and enjoy:)

# mysql and phpmyadmin
1.1 you can cd to docker directory and make your container with these commands
    
    cd docker
    docker compose up
    
1.2 if you want to get into ftp backup, i recommend you to use my Dockerfile to build your image
    
    cd docker
    docker build -t vsftpim ./
and run it with
      
    docker run -d -p 20:20 -p 21:21 -p 21100-21110:21100-21110 \
    -e FTP_USER=ansible -e FTP_PASS=ansible1234 \
    -e PASV_ADDRESS=127.0.0.1 -e PASV_MIN_PORT=21100 -e PASV_MAX_PORT=21110 \
    --name vsftpd  --restart=always fauria/vsftpd
# my social medias


my youtube = [https://b2n.ir/khodekhadem](b2n.ir/khodekhadem)  &nbsp;  [![click](https://img.shields.io/badge/-click%20!-420ACD)](https://b2n.ir/khodekhadem)

my telegram = [@khodekhadem](https://t.me/khodekhadem)  &nbsp;  [![click](https://img.shields.io/badge/-click%20!-420ACD)](https://t.me/khodekhadem)

my instagram = [khodekhadem](www.instagram.com/khodekhadem)  &nbsp;  [![click](https://img.shields.io/badge/-click%20!-420ACD)](https://www.instagram.com/khodekhadem)

my email = khadem13khadem@gmail.com


