module com.example.test9_p {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.desktop;
    requires java.sql;


    opens com.example.test9_p to javafx.fxml;
    exports com.example.test9_p;
}