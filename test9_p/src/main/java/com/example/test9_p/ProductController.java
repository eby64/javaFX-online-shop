package com.example.test9_p;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.net.URL;
import java.util.ResourceBundle;

public class ProductController implements Initializable {
    @FXML
    Label ProductCategoryLabel;

    @FXML
    Label ProductDescriptionLabel;

    @FXML
    Label ProductNameLabel;

    @FXML
    Label ProductRateLabel;

    @FXML
    Label productBrandLabel;

    @FXML
    ImageView productImageView;
    @FXML
    Label productPriceLabel;

    private String name;
    protected double price;
    private Image img;
    private double rate;
    private String category;
    private String brand;
    private int number;
    private  int id;
    public ProductController(){

    }
    public void setProductController(String name, double price,  Image img, double rate,String category, String brand, int number, int id) {
        this.name = name;
        this.price = price;
        this.rate = rate;
        this.img = img;
        this.category = category;
        this.brand = brand;
        this.number = number;
        this.id = id;
    }
    public Image getImg() {
        return img;
    }

    public void setImg(Image img) {
        this.img = img;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public double getRate() {
        return rate;
    }
    public void setRate(double rate) {
        this.rate = rate;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ProductNameLabel.setText(this.name);
        productBrandLabel.setText("brand: " + this.brand);
        ProductRateLabel.setText("rate: " + this.rate);
        ProductCategoryLabel.setText("category: " + this.category);
        productImageView.setImage(this.img);
        ProductDescriptionLabel.setText("nothing");
    }
}
