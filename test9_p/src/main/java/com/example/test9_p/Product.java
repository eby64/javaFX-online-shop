package com.example.test9_p;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.net.URL;
import java.util.ResourceBundle;

public class Product implements Initializable {
    @FXML
    Label ProductCategoryLabel;

    @FXML
    Label ProductDescriptionLabel;

    @FXML
    Label ProductNameLabel;

    @FXML
    Label ProductRateLabel;

    @FXML
    Label productBrandLabel;

    @FXML
    ImageView productImageView;
    @FXML
    Label productPriceLabel;

    String name = null;
    protected double price = 0;
    Image img;
    double rate = 0;
    String category = null;
    String brand = null;
    int number = 0;
    int id = 0;
    //private String producer;
    /*private int calories;
    private Date date_of_manufacture;
    private  Date expiration;
    private Date date_of_adding;
    private String description;
    private Seller seller;
    private Comment comments[];
    private Cellar cellar;*/
    public Product(){

    }
    public Product(String name, double price,  Image img, double rate,String category, String brand, int number, int id) {
        this.name = name;
        this.price = price;
        this.rate = rate;
        this.img = img;
        this.category = category;
        this.brand = brand;
        this.number = number;
        this.id = id;
    }

    public Image getImg() {
        return img;
    }

    public void setImg(Image img) {
        this.img = img;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    /*public String getProducer() {
        return producer;
    }
    public void setProducer(String producer) {
        this.producer = producer;
    }
    public int getCalories() {
        return calories;
    }
    public void setCalories(int calories) {
        this.calories = calories;
    }
    public Date getDate_of_manufacture() {
        return date_of_manufacture;
    }
    public void setDate_of_manufacture(Date date_of_manufacture) {
        this.date_of_manufacture = date_of_manufacture;
    }
    public Date getExpiration() {
        return expiration;
    }
    public void setExpiration(Date expiration) {
        this.expiration = expiration;
    }
    public Date getDate_of_adding() {
        return date_of_adding;
    }
    public void setDate_of_adding(Date date_of_adding) {
        this.date_of_adding = date_of_adding;
    }*/
    public double getRate() {
        return rate;
    }
    public void setRate(double rate) {
        this.rate = rate;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ProductNameLabel.setText(getName());
        productBrandLabel.setText("brand: " + getBrand());
        ProductRateLabel.setText("rate: " + getRate());
        ProductCategoryLabel.setText("category: " + getCategory());
        productImageView.setImage(getImg());
        ProductDescriptionLabel.setText("nothing");
    }
}
