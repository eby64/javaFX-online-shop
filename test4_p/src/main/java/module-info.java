module com.example.test4_p {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;


    opens com.example.test4_p to javafx.fxml;
    exports com.example.test4_p;
}