package store;

public class Comment {
	private Perchaser perchaser;
	private Date date;
	private String text;
	private Product product;
	public Comment(String s,Date d,Perchaser p){
		this.setText(s);
		this.setDate(d);
		this.setPerchaser(p);
		
	}
	public Perchaser getPerchaser() {
		return perchaser;
	}
	public void setPerchaser(Perchaser perchaser) {
		this.perchaser = perchaser;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
    

}
