module com.example.test6_p {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;


    opens com.example.test6_p to javafx.fxml;
    exports com.example.test6_p;
}