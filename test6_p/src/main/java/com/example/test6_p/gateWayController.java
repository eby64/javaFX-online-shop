package com.example.test6_p;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;

import java.io.InputStream;
import java.net.URL;
import java.sql.*;
import java.sql.Date;
import java.time.LocalDate;
import java.util.*;

public class gateWayController implements Initializable {

    Map<Integer, Integer> finalDataQuantity = new HashMap<>();
    Map<Integer, Product> finalDataProduct = new HashMap<>();
    Map<Integer, Integer> tempStockId = new HashMap<>();
    @FXML
    PasswordField CVV2;

    @FXML
    Label Captcha;

    @FXML
    TextField CardNumber;

    @FXML
    PasswordField CardPassword;

    @FXML
    TextField Email;

    @FXML
    TextField ExpirationMonth;

    @FXML
    TextField ExpirationYear;

    @FXML
    Button PooyaPassword;

    @FXML
    TextField SecurityCode;

    @FXML
    TextField amountPayable;
    private String captchaText;
    private String tempPassword;
    private boolean isCardNumberValid(TextField textField){
        String input = textField.getText();
        String pattern = "\\d{12}";
        return input.matches(pattern);
    }
    private boolean isCVV2Valid(PasswordField passwordField){
        String input = passwordField.getText();
        String pattern = "\\d{4}";
        return input.matches(pattern);
    }
    private boolean isExpirationMonthValid(TextField textField){
        String input = textField.getText();
        String pattern = "^(0[1-9]|1[0-2])$";
        return input.matches(pattern);
    }
    private boolean isExpirationYearValid(TextField textField){
        String input = textField.getText();
        String pattern = "^[0-9][2-9]|[1-9][0-9]$";
        return input.matches(pattern);
    }
    private boolean isCaptchaValid(TextField textField){
        String input = textField.getText();
        if(input.equals( captchaText)){
            return true;
        }
        return false;
    }
    private boolean isPooyaPasswordValid(PasswordField passwordField){
        String input = passwordField.getText();
        if(input.equals( tempPassword)){
            return true;
        }
        return false;
    }
    private boolean isEmailValid(TextField textField){
        String input = textField.getText();
        String pattern = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}$";
        return input.matches(pattern);
    }
    private String generateCaptcha() {
        String chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 6; i++) {
            char c = chars.charAt(random.nextInt(chars.length()));
            sb.append(c);
        }
        return sb.toString();
    }
    private String generatePassword() {
        String chars = "1234567890";
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 7; i++) {
            char c = chars.charAt(random.nextInt(chars.length()));
            sb.append(c);
        }
        return sb.toString();
    }
    @FXML
    void Captcha(MouseEvent event) {
        captchaText = generateCaptcha();
        Captcha.setText(captchaText);
    }

    @FXML
    void PaymentCancel(ActionEvent event) {

    }

    @FXML
    void PaymentSubmit(ActionEvent event) {
        System.out.println(isCardNumberValid(CardNumber));
        System.out.println(isCVV2Valid(CVV2));
        System.out.println(isExpirationMonthValid(ExpirationMonth));
        System.out.println(isExpirationYearValid(ExpirationYear));
        System.out.println(isCaptchaValid(SecurityCode));
        System.out.println(isPooyaPasswordValid(CardPassword));
        System.out.println(isEmailValid(Email));
        if(!isCardNumberValid(CardNumber) || !isCVV2Valid(CVV2) || !isExpirationMonthValid(ExpirationMonth) || !isExpirationYearValid(ExpirationYear) || !isPooyaPasswordValid(CardPassword) || !isEmailValid(Email) || !isCaptchaValid(SecurityCode)){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("نامعتبر");
            alert.setHeaderText(null);
            alert.setContentText("اطلاعات نامعتبر!");
            alert.showAndWait();
        }
        else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("موفقیت آمیز");
            alert.setHeaderText(null);
            alert.setContentText("خرید شما با موفقیت انجام شد!");
            alert.showAndWait();
            updateDatabase();
        }
    }

    @FXML
    void password(ActionEvent event) {
        CardPassword.setDisable(false);
        tempPassword = generatePassword();
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("رمز پویا");
        alert.setHeaderText(null);
        alert.setContentText("رمز پویا با موفقیت ارسال شد!" + "\n" + tempPassword);
        alert.showAndWait();
    }
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        captchaText =generateCaptcha();
        Captcha.setText(captchaText);
        amountPayable.setText(String.valueOf(PaymentController.price * 10));
        CardPassword.setDisable(true);
    }
    private void updateDatabase(){
        finalDataQuantity.putAll(CartController.dataQuantity);
        finalDataProduct.putAll(CartController.dataProduct);
        try {
            Class.forName("org.mariadb.jdbc.Driver").newInstance();
            String myUrl = "jdbc:mariadb://localhost:3306/test1?user=root";
            Connection connect = DriverManager.getConnection(myUrl);
            String query = "select product_id, stock_id from entrances";
            PreparedStatement state = connect.prepareStatement(query);
            ResultSet result = state.executeQuery();
            while (result.next()){
                int id  = result.getInt("product_id");
                int stock  = result.getInt("stock_id");
                tempStockId.put(id,stock);
            }
            state.close();
            connect.close();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        try {
            Class.forName("org.mariadb.jdbc.Driver").newInstance();
            String myUrl = "jdbc:mariadb://localhost:3306/test1?user=root";
            Connection connect = DriverManager.getConnection(myUrl);
            String query = "update products set number = ? where id = ?";
            PreparedStatement state = connect.prepareStatement(query);
            for (Map.Entry<Integer, Integer> entry : finalDataQuantity.entrySet()) {
                int id = entry.getKey();
                int data = entry.getValue();
                // set the parameters of the PreparedStatement with the values to be updated in the table
                state.setInt(1, finalDataProduct.get(id).getNumber() - data);
                state.setInt(2, id);
                // execute the update query and get the number of rows affected
                state.executeUpdate();
            }
            state.close();
            connect.close();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        try {
            Class.forName("org.mariadb.jdbc.Driver").newInstance();
            String myUrl = "jdbc:mariadb://localhost:3306/test1?user=root";
            Connection connect = DriverManager.getConnection(myUrl);
            String query = "insert into sells (product_id,quantity,date,bank_account,stock_id) values (?,?,?,?,?)";
            PreparedStatement state = connect.prepareStatement(query);
            for (Map.Entry<Integer, Integer> entry : finalDataQuantity.entrySet()) {
                int id = entry.getKey();
                int data = entry.getValue();
                // set the parameters of the PreparedStatement with the values to be updated in the table
                state.setInt(1, id);
                state.setInt(2, data);
                LocalDate localDate = LocalDate.now();
                Date date = Date.valueOf(localDate);
                state.setDate(3,date);
                switch (PaymentController.GateWay) {
                    case("mellat"):
                        state.setString(4, "ملت");
                        break;
                }
                state.setInt(5,tempStockId.get(id));
                state.executeUpdate();
            }
            state.close();
            connect.close();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
}
