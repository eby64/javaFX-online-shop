package com.example.test6_p;

import javafx.beans.InvalidationListener;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableArray;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.*;
import java.util.*;

public class CartController implements Initializable {
    List<Product> inCartProducts = new ArrayList<>();
    public static Map<Integer, Integer> dataQuantity = new HashMap<>();
    public static Map<Integer, Product> dataProduct = new HashMap<>();
    @FXML
    GridPane gridPane;
    @FXML
    Label subTotalPrice;
    @FXML
    Label taxPrice;
    @FXML
    Label totalPrice;
    int rowNum = inCartProducts.size();
    int currentQuantity;
    public static double ultimate;
    double sum = 0;
    Stage stage;
    Scene scene;
    public double temp ;
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle){
        try {
            Class.forName("org.mariadb.jdbc.Driver").newInstance();
            String myUrl = "jdbc:mariadb://localhost:3306/test1?user=root";
            Connection connect = DriverManager.getConnection(myUrl);
            String query = "select * from products";
            PreparedStatement state = connect.prepareStatement(query);
            ResultSet result = state.executeQuery();
            while (result.next()){
                String name = result.getString(1);
                double price = result.getDouble(2);
                Blob blob = result.getBlob(3);
                double rate = result.getDouble(4);
                InputStream in = blob.getBinaryStream();
                Image img = new Image(in);
                String category = result.getString(5);
                int number = result.getInt(6);
                int id = result.getInt("id");
                Product product = new Product(name,price,img,rate,category,number,id);
                inCartProducts.add(product);
                dataQuantity.put(id,1);
                dataProduct.put(id,product);
            }
            state.close();
            connect.close();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        for(int i=1 ; i < rowNum; i++){
            gridPane.addRow(i);
        }
        gridPane.setVgap(10);
        try {
            int j=0;
            int k=0;
            for (int i = 0; i < inCartProducts.size(); i++) {
                Product product = inCartProducts.get(i);
                VBox imgVbox = new VBox();
                ImageView imageView = new ImageView();
                imageView.setImage(product.getImg());
                imageView.setFitWidth(100);
                imageView.setFitHeight(100);
                imgVbox.getChildren().add(imageView);
                imgVbox.setAlignment(Pos.CENTER);
                imgVbox.setStyle("-fx-background-color: #446386");
                gridPane.add(imgVbox,j,k);
                VBox vbox = new VBox();
                vbox.setSpacing(10);
                Label nameLabel = new Label("نام کالا: " + product.getName());
                nameLabel.setFont(new Font(15));
                Label priceLabel = new Label("قیمت کالا: " + product.getPrice());
                priceLabel.setFont(new Font(15));
                HBox hbox= new HBox();
                Label quantityLabel = new Label("تعداد: ");
                quantityLabel.setFont(new Font(15));
                Spinner spinner = new Spinner<Integer>();
                SpinnerValueFactory<Integer> valueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(1,product.getNumber());
                valueFactory.setValue(1);
                spinner.setValueFactory(valueFactory);
                spinner.setPrefWidth(70);
                currentQuantity = (int) spinner.getValue();
                hbox.setSpacing(5);
                hbox.setAlignment(Pos.TOP_RIGHT);
                hbox.getChildren().addAll(spinner,quantityLabel);
                Label totalLabel = new Label("قیمت کل: " + currentQuantity * product.getPrice());
                totalLabel.setFont(new Font(15));
                spinner.valueProperty().addListener(new ChangeListener() {
                    @Override
                    public void changed(ObservableValue observableValue, Object o, Object t1) {
                        sum -= currentQuantity * product.getPrice();
                        currentQuantity = (int) spinner.getValue();
                        totalLabel.setText("قیمت کل: " + currentQuantity * product.getPrice());
                        sum += currentQuantity * product.getPrice();
                        subTotalPrice.setText(String.valueOf(sum));
                        taxPrice.setText(String.valueOf(sum * 0.09));
                        temp = sum + sum * 0.09;
                        totalPrice.setText(String.valueOf(temp));
                        ultimate = temp;
                        dataQuantity.put(product.getId(),currentQuantity);
                        //ultimate = Double.parseDouble(totalPrice.getText());
                        //System.out.println(ultimate);
                    }
                });
                vbox.getChildren().addAll(nameLabel,priceLabel,hbox,totalLabel);
                vbox.setAlignment(Pos.TOP_RIGHT);
                vbox.setStyle("-fx-background-color: #446386");
                gridPane.add(vbox,++j,k);
                j=0;
                k++;
                sum += currentQuantity * product.getPrice();
            }
            subTotalPrice.setText(String.valueOf(sum));
            taxPrice.setText(String.valueOf(sum * 0.09));
            temp = sum + sum * 0.09;
            totalPrice.setText(String.valueOf(temp));
            ultimate = temp;
            //ultimate = Double.parseDouble(totalPrice.getText());
            //System.out.println(ultimate);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }


    @FXML
    private void goToPayment(ActionEvent event) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("Payment.fxml"));
        stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(fxmlLoader.load());
        stage.setX(320);
        stage.setY(70);
        stage.setScene(scene);
        stage.show();
    }
}