package com.example.test6_p;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PaymentController implements Initializable {
    @FXML
    TextArea addressField;

    @FXML
    TextField emailField;

    @FXML
    Label invalidAddress;

    @FXML
    Label invalidEmail;

    @FXML
    Label invalidMatch;

    @FXML
    Label invalidName;

    @FXML
    Label invalidPassword;

    @FXML
    TextField nameField;

    @FXML
    CheckBox peik;

    @FXML
    CheckBox pishtaz;

    @FXML
    CheckBox policy;
    @FXML
    TextField couponField;
    @FXML
    Label priceLabel;
    @FXML
    Button confirmButton;
    @FXML
    RadioButton mellatButton;
    @FXML
    RadioButton melliButton;
    public static String GateWay;
    public static double price = CartController.ultimate;
    int couponFlag = 0;
    private Stage stage;
    private Scene scene;
    int confirmFlag = 0;
    @FXML
    private void handleGateWay(){
        if(mellatButton.isSelected()){
            GateWay = "mellat";
            confirmButton.setDisable(false);
        }
        if(melliButton.isSelected()){
            GateWay = "melli";
            confirmButton.setDisable(false);
        }
    }
    @FXML
    private void handleConfirmButton(ActionEvent event) throws IOException {
        if (nameField.getText().isEmpty()) {
            invalidName.setVisible(true);
            confirmFlag = 1;
        } else {
            invalidName.setVisible(false);
        }
        if (addressField.getText().isEmpty()) {
            invalidAddress.setVisible(true);
            couponFlag = 1;
        } else invalidAddress.setVisible(false);
        if (emailField.getText().isEmpty()) {
            invalidEmail.setVisible(true);
            confirmFlag = 1;

        } else {
            if (isValidEmail(emailField)) {
                invalidEmail.setVisible(false);
            } else {
                invalidEmail.setText("Invalid Email Address");
                invalidEmail.setVisible(true);
                confirmFlag = 1;
            }
        }
        if(confirmFlag == 0) {
            if (GateWay.equals("mellat")) {
                FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("MellatPortal.fxml"));
                stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                scene = new Scene(fxmlLoader.load());
                stage.setX(320);
                stage.setY(70);
                stage.setScene(scene);
                stage.show();
            }
            if(GateWay .equals("melli")){
                FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("MelliPortal.fxml"));
                stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                scene = new Scene(fxmlLoader.load());
                stage.setX(320);
                stage.setY(70);
                stage.setScene(scene);
                stage.show();
            }
        }
        confirmFlag = 0;
    }
    private static final Pattern EMAIL_PATTERN = Pattern.compile(
            "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}$"
    );

    // Method to check if the email is valid
    public boolean isValidEmail(TextField emailField) {
        String email = emailField.getText();
        Matcher matcher = EMAIL_PATTERN.matcher(email);
        return matcher.matches();
    }
    private static final Pattern COUPON_PATTERN = Pattern.compile("^[A-Z0-9]{5}$");

    // Method to check if the coupon code is valid in format
    public boolean isValidCouponFormat(String couponCode) {
        Matcher matcher = COUPON_PATTERN.matcher(couponCode);
        return matcher.matches();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //price = cart.getTotalPrice();
        emailField.setDisable(true);
        addressField.setDisable(true);
        couponField.setDisable(true);
        policy.setDisable(true);
        confirmButton.setDisable(true);
        mellatButton.setDisable(true);
        melliButton.setDisable(true);
        priceLabel.setText(String.valueOf(price));
        nameField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.isEmpty()) {
                emailField.setDisable(false);
            }
        });

        emailField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.isEmpty()) {
                addressField.setDisable(false);
            }
        });

        addressField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.isEmpty()) {
                couponField.setDisable(false);
                policy.setDisable(false);
            }
        });
        policy.setOnAction(event -> {
            if (policy.isSelected()) {
                //confirmButton.setDisable(false);
                mellatButton.setDisable(false);
                melliButton.setDisable(false);
            } else {
                mellatButton.setDisable(true);
                melliButton.setDisable(true);
            }
        });
        handleGateWay();
    }
    @FXML
    private void handleCoupon(){
        if (couponField.getText().isEmpty()) {
            invalidMatch.setVisible(true);

        } else {
            if (isValidCouponFormat(couponField.getText()) && couponFlag == 0) {
                invalidMatch.setVisible(false);
                price *= 0.9;
                priceLabel.setText(String.valueOf(price));
                couponFlag = 1;
            } else {
                invalidMatch.setText("Invalid Coupon code!");
                invalidMatch.setVisible(true);
            }
        }
    }
}
