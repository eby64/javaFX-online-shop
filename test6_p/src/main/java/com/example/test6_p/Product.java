package com.example.test6_p;

import javafx.scene.image.Image;

public class Product{
    private String name;
    private double price;
    private Image img;
    private double rate;
    private String category;
    private int number;
    private int id;
    //private String producer;
    /*private int calories;
    private Date date_of_manufacture;
    private  Date expiration;
    private Date date_of_adding;
    private String description;
    private Seller seller;
    private Comment comments[];
    private Cellar cellar;*/

    public Product(String name, double price,  Image img, double rate,String category,int number,int id) {
        this.name = name;
        this.price = price;
        this.rate = rate;
        this.img = img;
        this.category = category;
        this.number = number;
        this.id = id;
    }
    public Product(String name, double price,  Image img) {
        this.name = name;
        this.price = price;
        this.img = img;
    }

    public Image getImg() {
        return img;
    }

    public void setImg(Image img) {
        this.img = img;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    /*public String getProducer() {
        return producer;
    }
    public void setProducer(String producer) {
        this.producer = producer;
    }
    public int getCalories() {
        return calories;
    }
    public void setCalories(int calories) {
        this.calories = calories;
    }
    public Date getDate_of_manufacture() {
        return date_of_manufacture;
    }
    public void setDate_of_manufacture(Date date_of_manufacture) {
        this.date_of_manufacture = date_of_manufacture;
    }
    public Date getExpiration() {
        return expiration;
    }
    public void setExpiration(Date expiration) {
        this.expiration = expiration;
    }
    public Date getDate_of_adding() {
        return date_of_adding;
    }
    public void setDate_of_adding(Date date_of_adding) {
        this.date_of_adding = date_of_adding;
    }*/
    public double getRate() {
        return rate;
    }
    public void setRate(double rate) {
        this.rate = rate;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
