package com.example.test7_p;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Popup;
import javafx.stage.Screen;
import javafx.stage.Stage;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.*;
import java.util.*;

import javafx.collections.ObservableList;
import javafx.stage.Window;


public class HelloController implements Initializable{
    @FXML
    GridPane gridPane;
    @FXML
    ListView <String> listView1;
    @FXML
    ListView <String> listView2;
    List<Product> products = new ArrayList<>();
    List<Product> temp1Products = new ArrayList<>();
    List<Product> firstSort = new ArrayList<>();
    ObservableList <String> categoryList = FXCollections.observableArrayList();
    ObservableList <String> sortList = FXCollections.observableArrayList();
    private Stage stage;
    private Scene scene;
    Set<String> optionsSet = new HashSet<>();
    int rowNum;
    @FXML
    Button brandButton;
    List<String> filterList = new ArrayList<>();
    ListView<String> brandListView = new ListView<>();
    @FXML
    private void showBrands(ActionEvent event) throws IOException {
        Popup popup = new Popup();
        VBox popupContent = new VBox();
        popupContent.setAlignment(Pos.CENTER);
        Button exitButton = new Button("close");
        exitButton.setOnAction(event1 -> {
            //brandFunction();
            if(filterList.size() == 0) {
                filterList.clear();
            }
            filterList = brandListView.getSelectionModel().getSelectedItems();
            popup.hide();
        });
        popupContent.getChildren().addAll(exitButton,brandListView);
        popup.getContent().add(popupContent);
        Window ownerWindow = brandButton.getScene().getWindow();
        popup.show(ownerWindow);
    }
    @FXML
    private void applyFilter(ActionEvent event) throws IOException {
        gridPane.getChildren().clear();
        try {
            int j = 0;
            int k = 0;
            if (listView2.getSelectionModel().getSelectedItem() != null) {
                switch (listView2.getSelectionModel().getSelectedItem()) {
                    case "گران ترین":
                        products.sort(Comparator.comparing(Product::getPrice).reversed());
                        break;
                    case "ارزان ترین":
                        products.sort(Comparator.comparing(Product::getPrice));
                        break;
                    case "امتیاز":
                        products.sort(Comparator.comparing(Product::getRate).reversed());
                        break;
                    case "تاریخ اضافه شدن":
                        break;
                }
            }
            for (int i = 0; i < products.size(); i++) {
                if (j == 3) {
                    j = 0;
                    k++;
                }
                Product product = products.get(i);
                if (listView1.getSelectionModel().getSelectedItem() != null) {
                    String currentCategory = listView1.getSelectionModel().getSelectedItem();
                    if(currentCategory == "نمایش همه"){
                        //continue the current iteration
                    }
                    else if(!(product.getCategory().equals(currentCategory))){
                        continue;
                    }
                }
                if(!(filterList.isEmpty()) && !(filterList.contains(product.getBrand()))){
                    continue;
                }
                VBox vbox = new VBox();
                ImageView imageView = new ImageView();
                imageView.setImage(product.getImg());
                imageView.setFitWidth(150);
                imageView.setFitHeight(150);
                Label nameLabel = new Label(product.getName());
                nameLabel.setFont(new Font(16));
                Label priceLabel = new Label("قیمت: " + (int) product.getPrice());
                priceLabel.setFont(new Font(13));
                Label rateLabel = new Label("امتیاز: " + product.getRate());
                rateLabel.setFont(new Font(13));
                Button button = new Button("انتخاب ");
                vbox.getChildren().addAll(imageView, nameLabel, priceLabel, rateLabel, button);
                vbox.setAlignment(Pos.CENTER);
                vbox.setStyle("-fx-background-color: #446386");
                //vbox.setSpacing(10);
                gridPane.add(vbox, j, k);
                j++;
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }
    @FXML
    public void switchToAddProduct(ActionEvent event) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("AddProduct.fxml"));
        stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        stage.setX(550);
        stage.setY(180);
        scene = new Scene(fxmlLoader.load());
        stage.setScene(scene);
        stage.show();
    }
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        gridPane.setHgap(10);
        gridPane.setVgap(20);
        try {
            Class.forName("org.mariadb.jdbc.Driver").newInstance();
            String myUrl = "jdbc:mariadb://localhost:3306/test1?user=root";
            Connection connect = DriverManager.getConnection(myUrl);
            String query = "select * from products";
            PreparedStatement state = connect.prepareStatement(query);
            ResultSet result = state.executeQuery();
            while (result.next()){
                String name = result.getString(1);
                double price = result.getDouble(2);
                Blob blob = result.getBlob(3);
                double rate = result.getDouble(4);
                InputStream in = blob.getBinaryStream();
                Image img = new Image(in);
                String category = result.getString(5);
                String brand = result.getString("brand");
                int number = result.getInt("number");
                int id = result.getInt("id");
                if(number != 0) {
                    Product product = new Product(name, price, img, rate, category, brand, id, number);
                    products.add(product);
                    optionsSet.add(brand);
                }
            }
            state.close();
            connect.close();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        brandListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        brandListView.getSelectionModel().clearSelection();
        brandListView.getItems().addAll(optionsSet);
        if (products.size() <= 3){
            rowNum = 0;
        }
        else if(products.size() % 3 == 0){
            rowNum = products.size()/3;
        }
        else {
            rowNum = products.size()/3 + 1;
        }
        for(int i=1 ; i <= rowNum; i++){
            gridPane.addRow(i);
        }
        try {
            int j = 0;
            int k = 0;
            for (int i = 0; i < products.size(); i++) {
                if (j == 3) {
                    j = 0;
                    k++;
                }
                Product product = products.get(i);
                temp1Products.add(product);
                firstSort.add(product);
                VBox vbox = new VBox();
                ImageView imageView = new ImageView();
                imageView.setImage(product.getImg());
                imageView.setFitWidth(150);
                imageView.setFitHeight(150);
                Label nameLabel = new Label(product.getName());
                nameLabel.setFont(new Font(16));
                Label priceLabel = new Label("قیمت: " + (int) product.getPrice());
                priceLabel.setFont(new Font(13));
                Label rateLabel = new Label("امتیاز: " + product.getRate());
                rateLabel.setFont(new Font(13));
                Button button = new Button("انتخاب ");
                vbox.getChildren().addAll(imageView, nameLabel, priceLabel, rateLabel, button);
                vbox.setAlignment(Pos.CENTER);
                vbox.setStyle("-fx-background-color: #446386");
                //vbox.setSpacing(10);
                gridPane.add(vbox, j, k);
                j++;
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        categoryList.add("لبنیات");
        categoryList.add("پروتئین");
        categoryList.add("تنقلات");
        categoryList.add("نوشیدنی");
        categoryList.add("نمایش همه");
        listView1.getItems().addAll(categoryList);
        listView1.setCellFactory(param -> new ListCell<String>(){
            @Override
            protected void updateItem(String s, boolean b) {
                super.updateItem(s, b);
                if (b || s == null) {
                    setText(null);
                    setGraphic(null);
                } else {
                    if(isSelected()) {
                        setBackground(new Background(new BackgroundFill(Color.WHITE, null, null)));
                    }
                    else{
                        setBackground(new Background(new BackgroundFill(Color.web("#080b28"),null,null)));
                    }
                    setText(s);
                    setAlignment(Pos.CENTER_RIGHT);
                    setFont(new Font(15));
                    setPrefHeight(30);
                    setStyle("-fx-background-color: #080b28; -fx-text-fill: #12c99b");
                }
            }
        });
        listView1.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        listView1.getSelectionModel().selectedItemProperty().addListener((observableValue, s, t1) -> {
            String currentCategory = listView1.getSelectionModel().getSelectedItem();
            gridPane.getChildren().clear();
            temp1Products.clear();
            firstSort.clear();
            try {
                int j = 0;
                int k = 0;
                if(listView2.getSelectionModel().getSelectedItem() != null) {
                    switch (listView2.getSelectionModel().getSelectedItem()) {
                        case "گران ترین":
                            products.sort(Comparator.comparing(Product::getPrice).reversed());
                            break;
                        case "ارزان ترین":
                            products.sort(Comparator.comparing(Product::getPrice));
                            break;
                        case "امتیاز":
                            products.sort(Comparator.comparing(Product::getRate).reversed());
                            break;
                        case "تاریخ اضافه شدن":
                            break;
                    }
                }
                for (int i = 0; i < products.size(); i++) {
                    if (j == 3) {
                        j = 0;
                        k++;
                    }
                    Product product = products.get(i);
                    if (currentCategory == "نمایش همه") {
                        //continue the current iteration
                    } else if (!(product.getCategory().equals(currentCategory))) {
                        continue;
                    }
                    if (!(filterList.isEmpty()) && !(filterList.contains(product.getBrand()))) {
                        continue;
                    }
                    temp1Products.add(product);
                    firstSort.add(product);
                    VBox vbox = new VBox();
                    ImageView imageView = new ImageView();
                    imageView.setImage(product.getImg());
                    imageView.setFitWidth(150);
                    imageView.setFitHeight(150);
                    Label nameLabel = new Label(product.getName());
                    nameLabel.setFont(new Font(16));
                    Label priceLabel = new Label("قیمت: " + (int) product.getPrice());
                    priceLabel.setFont(new Font(13));
                    Label rateLabel = new Label("امتیاز: " + product.getRate());
                    rateLabel.setFont(new Font(13));
                    Button button = new Button("انتخاب ");
                    vbox.getChildren().addAll(imageView, nameLabel, priceLabel, rateLabel, button);
                    vbox.setAlignment(Pos.CENTER);
                    vbox.setStyle("-fx-background-color: #446386");
                    //vbox.setSpacing(10);
                    gridPane.add(vbox, j, k);
                    j++;
                }
            }
            catch (Exception e){
                e.printStackTrace();
            }
        });
        sortList.add("گران ترین");
        sortList.add("ارزان ترین");
        sortList.add("امتیاز");
        sortList.add("تاریخ اضافه شدن");
        listView2.getItems().addAll(sortList);
        listView2.setCellFactory(param -> new ListCell<String>(){
            @Override
            protected void updateItem(String s, boolean b) {
                super.updateItem(s, b);
                if (b || s == null) {
                    setText(null);
                    setGraphic(null);
                } else {
                    if(isSelected()) {
                        setBackground(new Background(new BackgroundFill(Color.WHITE, null, null)));
                    }
                    else{
                        setBackground(new Background(new BackgroundFill(Color.web("#080b28"),null,null)));
                    }
                    setText(s);
                    setAlignment(Pos.CENTER_RIGHT);
                    setFont(new Font(15));
                    setPrefHeight(30);
                    setStyle("-fx-background-color: #080b28; -fx-text-fill: #12c99b");
                }
            }
        });
        listView2.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        listView2.getSelectionModel().selectedItemProperty().addListener((observableValue, s, t1) -> {
            String currentSort = listView2.getSelectionModel().getSelectedItem();
            gridPane.getChildren().clear();
            switch (currentSort) {
                case "گران ترین":
                    temp1Products.sort(Comparator.comparing(Product::getPrice).reversed());
                    break;
                case "ارزان ترین":
                    temp1Products.sort(Comparator.comparing(Product::getPrice));
                    break;
                case "امتیاز":
                    temp1Products.sort(Comparator.comparing(Product::getRate).reversed());
                    break;
                case "تاریخ اضافه شدن":
                    int size = temp1Products.size();
                    temp1Products.clear();
                    for(int i=0; i<size; i++){
                        temp1Products.add(firstSort.get(i));
                    }
                    break;
            }
            try {
                gridPane.getChildren().clear();
                int j = 0;
                int k = 0;
                for (int i = 0; i < temp1Products.size(); i++) {
                    if (j == 3) {
                        j = 0;
                        k++;
                    }
                    Product product = temp1Products.get(i);
                    if(!(filterList.isEmpty()) && !(filterList.contains(product.getBrand()))){
                        continue;
                    }
                    VBox vbox = new VBox();
                    ImageView imageView = new ImageView();
                    imageView.setImage(product.getImg());
                    imageView.setFitWidth(150);
                    imageView.setFitHeight(150);
                    Label nameLabel = new Label(product.getName());
                    nameLabel.setFont(new Font(16));
                    Label priceLabel = new Label("قیمت: " + (int)product.getPrice());
                    priceLabel.setFont(new Font(13));
                    Label rateLabel = new Label("امتیاز: " + product.getRate());
                    rateLabel.setFont(new Font(13));
                    Button button = new Button("انتخاب ");
                    vbox.getChildren().addAll(imageView,nameLabel,priceLabel,rateLabel,button);
                    vbox.setAlignment(Pos.CENTER);
                    vbox.setStyle("-fx-background-color: #446386");
                    //vbox.setSpacing(10);
                    gridPane.add(vbox,j,k);
                    j++;
                }
            }
            catch (Exception e){
                e.printStackTrace();
            }
        });
    }
}