package com.example.test7_p;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.*;
import java.net.URL;
import java.sql.*;
import java.time.LocalDate;
import java.util.ResourceBundle;

public class AddProductController{
    private Stage stage;
    private Scene scene;
    @FXML
    ImageView imageView;
    @FXML
    RadioButton rButton1, rButton2, rButton3, rButton4;
    @FXML
    RadioButton r2Button1,r2Button2;
    @FXML
    TextField nameField;

    @FXML
    TextField numberField;

    @FXML
    TextField priceField;
    @FXML
    TextField brandField1;
    FileInputStream inputStream;
    String category;
    String account;
    int tempId;
    @FXML
    public void getCategory(ActionEvent event){
        if(rButton1.isSelected()) {
            category = rButton1.getText();
        }
        if(rButton2.isSelected()){
            category = rButton2.getText();
        }
        if(rButton3.isSelected()){
            category = rButton3.getText();
        }
        if(rButton4.isSelected()){
            category = rButton4.getText();
        }
    }
    @FXML
    public void getAccount(ActionEvent event){
        if(r2Button1.isSelected()) {
            account = r2Button1.getText();
        }
        if(r2Button2.isSelected()){
            account = r2Button2.getText();
        }
    }


    @FXML
    private void handleOpenImage(ActionEvent event) throws FileNotFoundException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Image File");
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"));
        File selectedFile = fileChooser.showOpenDialog(null);
        if (selectedFile != null) {
            inputStream = new FileInputStream(selectedFile);
            Image image = new Image(selectedFile.toURI().toString());
            imageView.setImage(image);
        }
    }
    @FXML
    private void handleReturn(ActionEvent event) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("SellerDashboard.fxml"));
        stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(fxmlLoader.load());
        stage.setX(320);
        stage.setY(70);
        stage.setScene(scene);
        stage.show();
    }
    @FXML
    private void handleUpload(ActionEvent event) {
        try {
            String name = nameField.getText();
            String brand = brandField1.getText();
            double price = Double.parseDouble(priceField.getText());
            int number = Integer.parseInt(numberField.getText());
            Class.forName("org.mariadb.jdbc.Driver").newInstance();
            String myUrl = "jdbc:mariadb://localhost:3306/test1?user=root";
            Connection connect = DriverManager.getConnection(myUrl);
            String query = "insert into products (name, price, image, category, number, brand) values (?,?,?,?,?,?)";
            PreparedStatement state = connect.prepareStatement(query);
            state.setString(1, name);
            state.setDouble(2, price);
            state.setBlob(3, inputStream);
            //state.setDouble(2, price);
            state.setString(4, category);
            state.setInt(5,number);
            state.setString(6,brand);
            state.executeUpdate();
            state.close();
            connect.close();

            Connection connect2 = DriverManager.getConnection(myUrl);
            String query2 = "select id from products where name = ? and category = ?";
            PreparedStatement state2 = connect2.prepareStatement(query2);
            state2.setString(1, name);
            state2.setString(2, category);
            ResultSet result = state2.executeQuery();
            while (result.next()){
                tempId = result.getInt("id");
            }
            state2.close();
            connect2.close();

            Connection connect3 = DriverManager.getConnection(myUrl);
            String query3 = "insert into entrances (product_id, quantity, date, bank_account , stock_id) values (?,?,?,?,?)";
            PreparedStatement state3 = connect3.prepareStatement(query3);
            state3.setInt(1,tempId);
            state3.setInt(2,number);
            state3.setDate(3, Date.valueOf(LocalDate.now()));
            state3.setString(4,account);
            state3.setInt(5,1);
            state3.executeUpdate();
            state3.close();
            connect3.close();

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Upload Successful");
            alert.setHeaderText(null);
            alert.setContentText("The information was uploaded successfully.");
            alert.showAndWait();
            FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("SellerDashboard.fxml"));
            stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
            scene = new Scene(fxmlLoader.load());
            stage.setX(320);
            stage.setY(70);
            stage.setScene(scene);
            stage.show();
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }
}
