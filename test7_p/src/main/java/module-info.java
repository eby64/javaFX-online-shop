module com.example.test7_p {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;
    requires java.desktop;


    opens com.example.test7_p to javafx.fxml;
    exports com.example.test7_p;
}