package com.example.test8_p;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.chart.*;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.Popup;
import javafx.stage.Window;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.time.LocalDate;
import java.time.Month;
import java.util.*;

public class AdminDashboardController implements Initializable{
    double[] totalSell = new double[12];
    double[] totalEntrance = new double[12];
    double[] categorySell = new double[4];
    @FXML
    RadioButton rButton1;
    @FXML
    RadioButton rButton2;
    @FXML
    RadioButton rButton3;

    @FXML
    LineChart <?,?> lineChart;
    @FXML
    CategoryAxis x;
    @FXML
    NumberAxis y;
    int selectedYear;
    XYChart.Series series1;
    XYChart.Series series2;
    int flagLineChart = 0;
    @FXML
    PieChart pieChart;
    int flagPieChart = 0;
    ObservableList<PieChart.Data> pieChartData;

    /////tab2
    @FXML
    DatePicker datePicker;
    @FXML
    Spinner yearSpinner;
    @FXML
    Spinner monthSpinner;
    @FXML
    Spinner year2Spinner;
    @FXML
    Spinner stockSpinner;
    @FXML
    BarChart<?,?> yearBarChart;
    @FXML
    BarChart <?,?> monthBarChart;
    @FXML
    CategoryAxis xYear;
    @FXML
    NumberAxis yYear;
    @FXML
    CategoryAxis xMonth;
    @FXML
    NumberAxis yMonth;
    @FXML
    Label dateLabel;

    Set<String> stocksNames = new HashSet<>();
    double[] totalSellStock = new double[12];
    double[] totalEntranceStock = new double[12];
    double[] totalStock = new double[12];
    double[] totalSellStock2 = new double[30];
    double[] totalEntranceStock2 = new double[30];
    double[] totalStock2 = new double[30];
    XYChart.Series yearSeries;
    int flagYearBarChart = 0;
    XYChart.Series monthSeries;
    /////tab3
    @FXML
    TextField textName1;
    @FXML
    TextField textName2;
    @FXML
    TextField textAddress1;
    @FXML
    TextField textAddress2;
    @FXML
    TextField textManager1;
    @FXML
    TextField textManager2;
    @FXML
    ChoiceBox<String> deleteChoiceBox;
    @FXML
    ChoiceBox<String> editChoiceBox;
    int deleteFlag = 0;
    @FXML
    private void handleLineChart(){
        x.setStartMargin(15);
        for(int i=0; i<totalSell.length; i++) {
            totalSell[i] = 0;
        }
        for(int i=0; i<totalEntrance.length; i++) {
            totalEntrance[i] = 0;
        }
        for(int i=0; i<categorySell.length; i++) {
            categorySell[i] = 0;
        }
        if(flagLineChart != 0){
            series1.getData().clear();
            lineChart.getData().clear();
        }
        if(flagPieChart != 0){
            pieChart.getData().clear();
            pieChartData.clear();
        }
        if(rButton1.isSelected()) {
            selectedYear = Integer.parseInt(rButton1.getText());
        }
        if(rButton2.isSelected()){
            selectedYear = Integer.parseInt(rButton2.getText());
        }
        if(rButton3.isSelected()){
            selectedYear = Integer.parseInt(rButton3.getText());
        }
        try {
            Class.forName("org.mariadb.jdbc.Driver").newInstance();
            String myUrl = "jdbc:mariadb://localhost:3306/test1?user=root";
            Connection connect = DriverManager.getConnection(myUrl);
            String query = "SELECT sells.quantity, products.category, sells.date, products.price from sells join products on sells.product_id = products.id where year(sells.date) = ?";
            PreparedStatement state = connect.prepareStatement(query);
            state.setInt(1,selectedYear);
            ResultSet result = state.executeQuery();
            while (result.next()){
                int quantity = result.getInt("quantity");
                double price = result.getDouble("price");
                String category = result.getString("category");
                LocalDate localDate = result.getDate("date").toLocalDate();
                totalSell[localDate.getMonth().getValue() - 1] += quantity*price;
                if(category != null) {
                    switch (category) {
                        case "لبنیات":
                            categorySell[0] += quantity * price;
                            break;
                        case "پروتئین":
                            categorySell[1] += quantity * price;
                            break;
                        case "تنقلات":
                            categorySell[2] += quantity * price;
                            break;
                        case "نوشیدنی":
                            categorySell[3] += quantity * price;
                            break;
                    }
                }
            }
            state.close();
            connect.close();
            series1 = new XYChart.Series();
            for(int i=1; i<=totalSell.length; i++) {
                Month month = Month.of(i);
                String monthName = month.name();
                series1.getData().add(new XYChart.Data(monthName, totalSell[i - 1]));
            }
            //lineChart.getData().addAll(series1);
            if(!isFullyEmpty(categorySell)) {
                pieChartData = FXCollections.observableArrayList(
                        new PieChart.Data("لبنیات", categorySell[0]),
                        new PieChart.Data("پروتئین", categorySell[1]),
                        new PieChart.Data("تنقلات", categorySell[2]),
                        new PieChart.Data("نوشیدنی", categorySell[3])
                );
                double total = 0;
                for(PieChart.Data data:pieChartData){
                    total += data.getPieValue();
                }
                for (PieChart.Data data : pieChartData) {
                    double percentage = (data.getPieValue() / total) * 100;
                    data.setName(data.getName() + " (" + String.format("%.2f", percentage) + "%)");
                }
                pieChart.setData(pieChartData);
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        try {
            Class.forName("org.mariadb.jdbc.Driver").newInstance();
            String myUrl = "jdbc:mariadb://localhost:3306/test1?user=root";
            Connection connect = DriverManager.getConnection(myUrl);
            String query = "SELECT entrances.quantity, entrances.date, products.price from entrances join products on entrances.product_id = products.id where year(entrances.date) = ?";
            PreparedStatement state = connect.prepareStatement(query);
            state.setInt(1,selectedYear);
            ResultSet result = state.executeQuery();
            while (result.next()) {
                int quantity = result.getInt("quantity");
                double price = result.getDouble("price");
                LocalDate localDate = result.getDate("date").toLocalDate();
                totalEntrance[localDate.getMonth().getValue() - 1] += quantity*(price*0.2);
            }
            series2 = new XYChart.Series();
            for(int i=1; i<=totalEntrance.length; i++) {
                Month month = Month.of(i);
                String monthName = month.name();
                series2.getData().add(new XYChart.Data(monthName,totalEntrance[i-1]));
            }
            lineChart.getData().addAll(series1,series2);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        flagLineChart = 1;
        if(isFullyEmpty(categorySell)){
           flagPieChart = 0;
        }
        else {
            flagPieChart = 1;
        }
    }
    public static boolean isFullyEmpty(double[] array) {
        for (double element : array) {
            if (element != 0) {
                return false;
            }
        }
        return true;
    }
    //////tab2
    private  void setStocksNames(){
        SpinnerValueFactory<Integer> valueFactory1 = new SpinnerValueFactory.IntegerSpinnerValueFactory(2021,2023);
        valueFactory1.setValue(2023);
        yearSpinner.setValueFactory(valueFactory1);
        SpinnerValueFactory<Integer> valueFactory2 = new SpinnerValueFactory.IntegerSpinnerValueFactory(2021,2023);
        valueFactory2.setValue(2023);
        year2Spinner.setValueFactory(valueFactory2);
        SpinnerValueFactory<Integer> valueFactory3 = new SpinnerValueFactory.IntegerSpinnerValueFactory(1,12);
        valueFactory3.setValue(1);
        monthSpinner.setValueFactory(valueFactory3);
        try {
            Class.forName("org.mariadb.jdbc.Driver").newInstance();
            String myUrl = "jdbc:mariadb://localhost:3306/test1?user=root";
            Connection connect = DriverManager.getConnection(myUrl);
            String query = "select name from stock";
            //SELECT entrances.quantity, entrances.date, stock.name, products.price from entrances join products on entrances.product_id = products.id join stock on entrances.stock_id
            PreparedStatement state = connect.prepareStatement(query);
            ResultSet result = state.executeQuery();
            while (result.next()) {
                stocksNames.add(result.getString("name"));
            }
            ObservableList<String> valuesList = FXCollections.observableArrayList(stocksNames);
            SpinnerValueFactory<String> valueFactory4 = new SpinnerValueFactory.ListSpinnerValueFactory<>(valuesList);
            stockSpinner.setValueFactory(valueFactory4);
            editChoiceBox.setItems(valuesList);
            deleteChoiceBox.setItems(valuesList);
            state.close();
            connect.close();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
    @FXML
    private void handleBarChart(){
        if(flagYearBarChart != 0){
            yearSeries.getData().clear();
            yearBarChart.getData().clear();
            monthSeries.getData().clear();
            monthBarChart.getData().clear();
        }
        for(int i=0; i<30; i++) {
            totalSellStock2[i] = 0;
            totalEntranceStock2[i] = 0;
            totalStock2[i] = 0;
        }
        for(int i=0; i<12; i++) {
            totalSellStock[i] = 0;
            totalEntranceStock[i] = 0;
            totalStock[i] = 0;
        }
        try {
            Class.forName("org.mariadb.jdbc.Driver").newInstance();
            String myUrl = "jdbc:mariadb://localhost:3306/test1?user=root";
            Connection connect = DriverManager.getConnection(myUrl);
            String query = "SELECT sells.quantity, sells.date, products.price, stock.name from sells join products on sells.product_id = products.id join stock on sells.stock_id = stock.id where year(sells.date) < ?";
            PreparedStatement state = connect.prepareStatement(query);
            state.setInt(1, (int)yearSpinner.getValue()+1);
            ResultSet result = state.executeQuery();
            int sum = 0;
            while (result.next()) {
                LocalDate localDate = result.getDate("date").toLocalDate();
                if(stockSpinner.getValue().equals(result.getString("name")) && localDate.getYear() < (int)yearSpinner.getValue()) {
                    int quantity = result.getInt("quantity");
                    double price = result.getDouble("price");
                    sum += quantity * price;
                }
            }
            result.first();
            while (result.next()) {
                System.out.println("5555555");
                LocalDate localDate = result.getDate("date").toLocalDate();
                if(stockSpinner.getValue().equals(result.getString("name")) && localDate.getYear() == (int)yearSpinner.getValue()) {
                    int quantity = result.getInt("quantity");
                    double price = result.getDouble("price");
                    totalSellStock[localDate.getMonth().getValue() - 1] += (quantity * price);
                }
            }
            state.close();
            connect.close();
            for(int i=1; i<12; i++){
                totalSellStock[i] += totalSellStock[i - 1];
            }
            for(int i=0; i<12; i++){
                totalSellStock[i] += sum;
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        try {
            Class.forName("org.mariadb.jdbc.Driver").newInstance();
            String myUrl = "jdbc:mariadb://localhost:3306/test1?user=root";
            Connection connect = DriverManager.getConnection(myUrl);
            String query = "SELECT entrances.quantity, entrances.date, products.price, stock.name from entrances join products on entrances.product_id = products.id join stock on entrances.stock_id = stock.id where year(entrances.date) < ?";
            PreparedStatement state = connect.prepareStatement(query);
            state.setInt(1, (int)yearSpinner.getValue() + 1);
            ResultSet result = state.executeQuery();
            int sum = 0;
            while (result.next()) {
                System.out.println("111111111111");
                LocalDate localDate = result.getDate("date").toLocalDate();
                if(stockSpinner.getValue().equals(result.getString("name")) && localDate.getYear() < (int)yearSpinner.getValue()) {
                    int quantity = result.getInt("quantity");
                    double price = result.getDouble("price");
                    sum += quantity * price;
                }
            }
            result.first();
            while (result.next()) {
                System.out.println("222222222222");
                LocalDate localDate = result.getDate("date").toLocalDate();
                if(stockSpinner.getValue().equals(result.getString("name")) && localDate.getYear() == (int)yearSpinner.getValue()) {
                    int quantity = result.getInt("quantity");
                    double price = result.getDouble("price");
                    totalEntranceStock[localDate.getMonth().getValue() - 1] += (quantity * price);
                }
            }
            state.close();
            connect.close();
            for(int i=1; i<12; i++){
                totalEntranceStock[i] += totalEntranceStock[i - 1];
            }
            for(int i=0; i<12; i++){
                totalEntranceStock[i] += sum;
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        for(int i=0; i<12; i++) {
            totalStock[i] = totalEntranceStock[i] - totalSellStock[i];
        }
        yearSeries = new XYChart.Series();
        for(int i=1; i<=12; i++) {
            //Month month = Month.of(i);
            //String monthName = month.name();
            yearSeries.getData().add(new XYChart.Data(String.valueOf(i),totalStock[i-1]));
        }
        yearBarChart.getData().add(yearSeries);
        flagYearBarChart = 1;

        ////////monthBarChart

        try {
            Class.forName("org.mariadb.jdbc.Driver").newInstance();
            String myUrl = "jdbc:mariadb://localhost:3306/test1?user=root";
            Connection connect = DriverManager.getConnection(myUrl);
            String query = "SELECT sells.quantity, sells.date, products.price, stock.name from sells join products on sells.product_id = products.id join stock on sells.stock_id = stock.id where year(sells.date) < ?  or (year(sells.date) = ? and month(sells.date)) <= ?";
            PreparedStatement state = connect.prepareStatement(query);
            state.setInt(1, (int)year2Spinner.getValue());
            state.setInt(2, (int)year2Spinner.getValue());
            state.setInt(3, (int)monthSpinner.getValue());
            ResultSet result = state.executeQuery();
            int sum = 0;
            while (result.next()) {
                LocalDate localDate = result.getDate("date").toLocalDate();
                if(stockSpinner.getValue().equals(result.getString("name")) && (localDate.getYear() < (int)year2Spinner.getValue() || (localDate.getYear() == (int)year2Spinner.getValue()) && localDate.getMonth().getValue() < (int)monthSpinner.getValue())) {
                    int quantity = result.getInt("quantity");
                    double price = result.getDouble("price");
                    sum += quantity * price;
                }
            }
            result.first();
            while (result.next()) {
                LocalDate localDate = result.getDate("date").toLocalDate();
                if(stockSpinner.getValue().equals(result.getString("name")) && (localDate.getYear() == (int)year2Spinner.getValue() && localDate.getMonth().getValue() == (int)monthSpinner.getValue() )) {
                    int quantity = result.getInt("quantity");
                    double price = result.getDouble("price");
                    totalSellStock2[localDate.getDayOfMonth() - 1] += (quantity * price);
                }
            }
            state.close();
            connect.close();
            for(int i=1; i<30; i++){
                totalSellStock2[i] += totalSellStock2[i - 1];
            }
            for(int i=0; i<30; i++){
                totalSellStock2[i] += sum;
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        try {
            Class.forName("org.mariadb.jdbc.Driver").newInstance();
            String myUrl = "jdbc:mariadb://localhost:3306/test1?user=root";
            Connection connect = DriverManager.getConnection(myUrl);
            String query = "SELECT entrances.quantity, entrances.date, products.price, stock.name from entrances join products on entrances.product_id = products.id join stock on entrances.stock_id = stock.id where year(entrances.date) < ?  or (year(entrances.date) = ? and month(entrances.date)) <= ?";
            PreparedStatement state = connect.prepareStatement(query);
            state.setInt(1, (int)year2Spinner.getValue());
            state.setInt(2, (int)year2Spinner.getValue());
            state.setInt(3, (int)monthSpinner.getValue());
            ResultSet result = state.executeQuery();
            int sum  = 0;
            while (result.next()) {
                LocalDate localDate = result.getDate("date").toLocalDate();
                if(stockSpinner.getValue().equals(result.getString("name")) && (localDate.getYear() < (int)year2Spinner.getValue() || (localDate.getYear() == (int)year2Spinner.getValue()) && localDate.getMonth().getValue() < (int)monthSpinner.getValue())) {
                    int quantity = result.getInt("quantity");
                    double price = result.getDouble("price");
                    sum += quantity * price;
                }
            }
            result.first();
            while (result.next()) {
                LocalDate localDate = result.getDate("date").toLocalDate();
                if(stockSpinner.getValue().equals(result.getString("name")) && (localDate.getYear() == (int)year2Spinner.getValue() && localDate.getMonth().getValue() == (int)monthSpinner.getValue() )) {
                    int quantity = result.getInt("quantity");
                    double price = result.getDouble("price");
                    totalEntranceStock2[localDate.getDayOfMonth() - 1] += (quantity * price);
                }
            }
            state.close();
            connect.close();
            for(int i=1; i<30; i++){
                totalEntranceStock2[i] += totalEntranceStock2[i - 1];
            }
            for(int i=0; i<30; i++){
                totalEntranceStock2[i] += sum;
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        for(int i=0; i<30; i++) {
            totalStock2[i] = totalEntranceStock2[i] - totalSellStock2[i];
        }
        monthSeries = new XYChart.Series();
        for(int i=1; i<=30; i++) {
            //Month month = Month.of(i);
            //String monthName = month.name();
            monthSeries.getData().add(new XYChart.Data(String.valueOf(i),totalStock2[i-1]));
        }
        monthBarChart.getData().add(monthSeries);
        //flagMonthBarChart = 1;
        if(datePicker.getValue() == null) {
            dateLabel.setText("");
        }
        else {
            stockPropertyDay();
            deleteFlag = 0;
        }
    }
    private void stockPropertyDay(){
        //else {
            if(datePicker.getValue() != null) {
                LocalDate selectedDate = datePicker.getValue();
            }
            int sellsSum = 0;
            int entrancesSum = 0;
            int sellsSum2 = 0;
            int entrancesSum2 = 0;
            try {
                Class.forName("org.mariadb.jdbc.Driver").newInstance();
                String myUrl = "jdbc:mariadb://localhost:3306/test1?user=root";
                Connection connect = DriverManager.getConnection(myUrl);
                String query = "SELECT sells.quantity, sells.date, products.price, stock.name from sells join products on sells.product_id = products.id join stock on sells.stock_id = stock.id where date <= ?";
                PreparedStatement state = connect.prepareStatement(query);
      //////////          state.setDate(1, java.sql.Date.valueOf(selectedDate));
                ResultSet result = state.executeQuery();
                while (result.next()) {
                    if (stockSpinner.getValue().equals(result.getString("name"))) {
                        int quantity = result.getInt("quantity");
                        double price = result.getDouble("price");
                        sellsSum += quantity * price;
                    }
                    if (deleteChoiceBox.getValue().equals(result.getString("name"))) {
                        int quantity2 = result.getInt("quantity");
                        double price2 = result.getDouble("price");
                        sellsSum2 += quantity2 * price2;
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                Class.forName("org.mariadb.jdbc.Driver").newInstance();
                String myUrl = "jdbc:mariadb://localhost:3306/test1?user=root";
                Connection connect = DriverManager.getConnection(myUrl);
                String query = "SELECT entrances.quantity, entrances.date, products.price, stock.name from entrances join products on entrances.product_id = products.id join stock on entrances.stock_id = stock.id where date <= ?";
                PreparedStatement state = connect.prepareStatement(query);
   ///////////////             state.setDate(1, java.sql.Date.valueOf(selectedDate));
                ResultSet result = state.executeQuery();
                while (result.next()) {
                    if (stockSpinner.getValue().equals(result.getString("name"))) {
                        int quantity = result.getInt("quantity");
                        double price = result.getDouble("price");
                        entrancesSum += quantity * price;
                    }
                    if (deleteChoiceBox.getValue().equals(result.getString("name"))) {
                        int quantity2 = result.getInt("quantity");
                        double price2 = result.getDouble("price");
                        entrancesSum2 += quantity2 * price2;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            dateLabel.setText(String.valueOf(entrancesSum - sellsSum));
            if(entrancesSum2 - sellsSum2 == 0) {
                deleteFlag = 1;
            }
        //}
    }
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        setStocksNames();
    }
    /////tab3
    @FXML
    private void addStock(){
        try{
            boolean allFieldsFilled1 = true;
            if (textName1.getText().isEmpty() || textAddress1.getText().isEmpty() || textManager1.getText().isEmpty()) {
                allFieldsFilled1 = false;
            }
            if(allFieldsFilled1) {
                Class.forName("org.mariadb.jdbc.Driver").newInstance();
                String myUrl = "jdbc:mariadb://localhost:3306/test1?user=root";
                Connection connect = DriverManager.getConnection(myUrl);
                String query = "INSERT INTO stock (name, address, manager) VALUES (?, ?, ?)";
                PreparedStatement state = connect.prepareStatement(query);
                state.setString(1, textName1.getText());
                state.setString(2, textAddress1.getText());
                state.setString(3, textManager1.getText());
                state.executeUpdate();
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Upload Successful");
                alert.setHeaderText(null);
                alert.setContentText("The information was uploaded successfully.");
                alert.showAndWait();
                state.close();
                connect.close();
                setStocksNames();
                textName1.clear();
                textManager1.clear();
                textAddress1.clear();
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
    @FXML
    private void editStock(){
        try{
            boolean allFieldsFilled2 = true;
            if (textName2.getText().isEmpty() || textAddress2.getText().isEmpty() || textManager2.getText().isEmpty() || editChoiceBox.getValue() == null) {
                allFieldsFilled2 = false;
            }
            if(allFieldsFilled2) {
                Class.forName("org.mariadb.jdbc.Driver").newInstance();
                String myUrl = "jdbc:mariadb://localhost:3306/test1?user=root";
                Connection connect = DriverManager.getConnection(myUrl);
                String query = "UPDATE stock SET name = ?, address = ? ,manager = ? WHERE name = ?";
                PreparedStatement state = connect.prepareStatement(query);
                state.setString(1, textName2.getText());
                state.setString(2, textAddress2.getText());
                state.setString(3, textManager2.getText());
                state.setString(4, editChoiceBox.getValue());
                state.executeUpdate();
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Upload Successful");
                alert.setHeaderText(null);
                alert.setContentText("The information was uploaded successfully.");
                alert.showAndWait();
                state.close();
                connect.close();
                setStocksNames();
                textName2.clear();
                textManager2.clear();
                textAddress2.clear();
            }

        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
    @FXML
    private void deleteStock(){
        try{
            boolean allFieldsFilled3 = true;
            boolean isStockEmpty = true;
            if (deleteChoiceBox.getValue() == null) {
                allFieldsFilled3 = false;
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error!");
                alert.setHeaderText(null);
                alert.setContentText("choose the stock!");
                alert.showAndWait();
            }
            stockPropertyDay();
            if(deleteFlag == 0){
                isStockEmpty = false;
            }
            if(allFieldsFilled3 && isStockEmpty) {
                Class.forName("org.mariadb.jdbc.Driver").newInstance();
                String myUrl = "jdbc:mariadb://localhost:3306/test1?user=root";
                Connection connect = DriverManager.getConnection(myUrl);
                String query = "delete stock where name = ?";
                PreparedStatement state = connect.prepareStatement(query);
                state.setString(1, deleteChoiceBox.getValue());
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("permission allowed");
                alert.setHeaderText(null);
                alert.setContentText("are you sure you want to delete the stock?");
                alert.showAndWait();
                state.executeUpdate();
                state.close();
                connect.close();
                setStocksNames();
            }

        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
    @FXML
    private void showAccounts() {
        Popup popup = new Popup();
        VBox popupContent = new VBox();
        popupContent.setAlignment(Pos.CENTER);
        Button exitButton = new Button("close");
        exitButton.setOnAction(event1 -> {
            popup.hide();
        });
        //Label accountNumber1 = new Label("شعبه حساب: تهران"+"نوع حساب: جاری"+"شماره حساب: 896327"+"\n"+"شعبه حساب: مشهد" + "نوع حساب: جاری"+ "شماره حساب: 549812");
        Label accountName1 = new Label("حساب اینده ");
        accountName1.setFont(Font.font("Arial", 17));
        Label accountNumber1 = new Label("شماره حساب: 549812 ");
        Label accountKind1 = new Label("نوع حساب: جاری");
        Label accountPlace1 = new Label("شعبه حساب: مشهد" + "\n");
        Label accountName2 = new Label("\n" + "حساب ملت " + "\n");
        accountName2.setFont(Font.font("\n" + "Arial", 17));
        Label accountNumber2 = new Label("شماره حساب: 896327");
        Label accountKind2 = new Label("نوع حساب: جاری");
        Label accountPlace2 = new Label("شعبه حساب: تهران");
        popupContent.setStyle("-fx-background-color: #446386;");
        popupContent.getChildren().addAll(exitButton, accountName1, accountNumber1, accountKind1, accountPlace1, accountName2, accountNumber2, accountKind2, accountPlace2);
        popup.getContent().add(popupContent);
        Window ownerWindow = textName1.getScene().getWindow();
        popup.show(ownerWindow);
    }
    @FXML
    private void getEntrancesCsv(ActionEvent event){
        Connection connect = null;
        try {
            Class.forName("org.mariadb.jdbc.Driver").newInstance();
            String myUrl = "jdbc:mariadb://localhost:3306/test1?useUnicode=yes&characterEncoding=UTF-8&user=root";
            connect = DriverManager.getConnection(myUrl);
            String query = "SELECT products.name , entrances.quantity, entrances.date, stock.name FROM entrances JOIN products ON entrances.product_id = products.id JOIN stock ON entrances.stock_id = stock.id";
            PreparedStatement state = connect.prepareStatement(query);
            ResultSet result = state.executeQuery();

            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Save CSV File");
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSV Files", "*.csv"));
            File file = fileChooser.showSaveDialog(textName1.getScene().getWindow());

            if (file != null) {
                FileWriter writer = new FileWriter(file);
                writer.append("products.name,quantity,date,stock.name\n");

                while (result.next()) {
                    String column1 = result.getString("products.name");
                    String column2 = result.getString("quantity");
                    String column3 = result.getString("date");
                    String column4 = result.getString("stock.name");

                    writer.append(String.format("%s,%s,%s,%s\n", column1, column2, column3, column4));
                }

                writer.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (connect != null) {
                try {
                    connect.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    @FXML
    private void getSellsCsv(){
        Connection connect = null;
        try {
            Class.forName("org.mariadb.jdbc.Driver").newInstance();
            String myUrl = "jdbc:mariadb://localhost:3306/test1?useUnicode=yes&characterEncoding=UTF-8&user=root";
            connect = DriverManager.getConnection(myUrl);
            String query = "SELECT products.name , sells.quantity, sells.date, stock.name FROM sells JOIN products ON sells.product_id = products.id JOIN stock ON sells.stock_id = stock.id";
            PreparedStatement state = connect.prepareStatement(query);
            ResultSet result = state.executeQuery();
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Save CSV File");
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSV Files", "*.csv"));
            File file = fileChooser.showSaveDialog(textName1.getScene().getWindow());
            if (file != null) {
                FileWriter writer = new FileWriter(file);
                writer.append("products.name,quantity,date,stock.name\n");
                while (result.next()) {
                    String column1 = result.getString("products.name");
                    String column2 = result.getString("quantity");
                    String column3 = result.getString("date");
                    String column4 = result.getString("stock.name");

                    writer.append(String.format("%s,%s,%s,%s\n", column1, column2, column3, column4));
                }

                writer.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (connect != null) {
                try {
                    connect.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
