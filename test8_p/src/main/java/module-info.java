module com.example.test8_p {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;


    opens com.example.test8_p to javafx.fxml;
    exports com.example.test8_p;
}